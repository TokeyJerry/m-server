
/**
 * @author Tie Jianwen
 * @date 2018-09-30
 * @name Entrance for server
 */

/**
 * Define variables
 */
import Koa from 'koa'

/**
 * Koa middleware
 */
import compose from 'koa-compose'
import onerror from 'koa-onerror'
import CookieParser from 'koa-cookie-parser'
import session from 'koa-session2'
import bodyparser from 'koa-bodyparser'
import logger from 'koa-logger'

/**
 * self middleware
 */
import errStatus from './Middlewares/err-status'

/**
 * Mount router middlewares
 */
import router from './Router'

const app = new Koa()

/**
 * Each `app.use()` only accepts a single generator function.
 * If you want to combine multiple generator functions into a single one,
 * you can use `koa-compose` to do so.
 * This allows you to use `app.use()` only once.
 * Your code will end up looking something like:
 *
 *   app.use(compose([
 *     function *(){},
 *     function *(){},
 *     function *(){}
 *   ]))
 */
app.use(compose([
  async (ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
  },
  CookieParser({
    cookieNameList: ['userId','uuId'],
    cipherKey: "hello world",
    maxAge: 60*60*24
  }),
  bodyparser({
    enableTypes:['json', 'form', 'text']
  }),
  session({
    key: "koa:sess",   //default "koa:sess"
  }),
  errStatus(),
  logger(),
  router.routes(),
  router.allowedMethods(),
]))

/**
 *
 */
onerror(app)

/**
 * error-handling
 */
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
})

/**
 * Exports app
 */
export { app }
