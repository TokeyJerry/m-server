/**
 *
 *
 */

import to from 'await-to-js';
import demo02, {Demo01_Shema} from '../Model/demo01/demo01Model';
import Model from '../Utils/model-util';
const userModel = new Model(demo02);

const userData = {
  name: 'Tie jianwen',
  email: 'jianwen_t@163.com',
  password: '123456',
};

const userData2 = {
  name: 'Tie jianwen1',
  email: 'jianwen_t@163.com',
  password: '131313',
};

const userData3 = {
  name: 'Tie jianwen2',
  email: 'jianwen_t@163.com',
  password: '121212',
};

const userData1 = {
  name: 'Tie jianwen3',
  email: 'jianwen_t@163.com',
  password: '123123',
};

module.exports = {
  async getIndex(ctx, next) {
    let [err, result] = await to(userModel.delete(...[{id: 3}]));
    if (err) {
      return ctx.e404({ code: 402, message: err })
    }
    ctx.body = {
      code: 0,
      data: result
    }
  }
}