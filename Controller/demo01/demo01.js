/**
 * @name controller
 * @type demo
 */

import to from 'await-to-js';
import demo01, {Demo01_Shema} from '../../Model/demo01/demo01Model';

/**
 * @name addOne
 * @description 单个新增
 * @type test
 */

export async function addOne(ctx, next) {
  let param = ctx.request.body || {};
  let [err, result] = await to(demo01.create(param))

  if (err) {
    return ctx.e404({ code: 402, message: err })
  }
  ctx.body = {
    code: 0,
    data: result
  }
}

/**
 * @name addBatch
 * @description 批量新增
 * @type test
 */

export async function addBatch(ctx, next) {
  let param = ctx.request.body || [];
  let [err, result] = await to(demo01.create(...param))

  if (err) {
    return ctx.e404({ code: 402, message: err })
  }
  ctx.body = {
    code: 0,
    data: result
  }
}

/**
 * @name deleteFn
 * @description 单个删除 or 批量删除
 * @type test
 */

export async function deleteFn(ctx, next) {
  let param = ctx.request.body || [];
  let [err, result] = await to(demo01.remove(param))

  if (err) {
    return ctx.e404({ code: 402, message: err })
  }
  ctx.body = {
    code: 0,
    data: result
  }
}

/**
 * @name modifyFn
 * @description 单个修改 or 批量修改
 * @type test
 */

export async function modifyFn(ctx, next) {
  let param = ctx.request.body || [];
  let [err, result] = await to(demo01.update({id: param.id }, param))

  if (err) {
    return ctx.e404({ code: 402, message: err })
  }
  ctx.body = {
    code: 0,
    data: result
  }
}

/**
 * @name findFn
 * @description 单个删除 or 批量删除
 * @type test
 */

export async function findFn(ctx, next) {
  let query = ctx.query || [];
  let [err, result] = await to(demo01.find(query))

  if (err) {
    return ctx.e404({ code: 402, message: err })
  }
  ctx.body = {
    code: 0,
    data: result
  }
}
