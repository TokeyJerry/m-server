
import Router from 'koa-router';
let demo01 = new Router({
  prefix: 'test/'
});

/**
 * @type test
 */
import {
  addOne,
  addBatch,
  deleteFn,
  modifyFn,
  findFn,
} from '../../Controller/demo01/demo01';

demo01.post('addOne', addOne)
    .post('addBatch', addBatch)
    .delete('deleteFn', deleteFn)
    .put('modifyFn', modifyFn)
    .get('findFn', findFn);

export default demo01;