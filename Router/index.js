/**
 * Router middlewares
 */

import Router from 'koa-router';
let router = new Router({
  prefix: '/'
});

/**
 * @name demo01
 * @description demo：add、delete、modify|update、find
 */
import demo01 from './demo01/demo01';
router.use(
    demo01.routes(),
    demo01.allowedMethods()
);

export default router
