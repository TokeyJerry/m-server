/**
 * @name addOneTest
 * @type {{name: string, email: string, password: string}}
 */
var addOneTest = {
  name: 'test name',
  email: 'test@163.com',
  password: '123456',
};

fetch('/addOne', {
  method: 'POST',
  body: JSON.stringify(addOneTest),
  headers: {'Content-Type': 'application/json' }
}).then((res) => {
  return res.json()
}).then((res) => {
  console.log(res)
})

/**
 * @name addBatchTest
 * @type {*[]}
 */
var addBatchTest = [{
  name: 'test name',
  email: 'test@163.com',
  password: '123456',
},{
  name: 'test name2',
  email: 'test2@163.com',
  password: '123123',
}];

fetch('/addOne', {
  method: 'POST',
  body: JSON.stringify(addBatchTest),
  headers: {'Content-Type': 'application/json' }
}).then((res) => {
  return res.json()
}).then((res) => {
  console.log(res)
})

/**
 * @name deleteFn
 * @type {*[]}
 */
var deleteFn = {
  id: 1
};

fetch('/deleteFn', {
  method: 'DELETE',
  body: JSON.stringify(deleteFn),
  headers: {'Content-Type': 'application/json' }
}).then((res) => {
  return res.json()
}).then((res) => {
  console.log(res)
})

/**
 * @name deleteFn
 * @type {*[]}
 */
var modifyFn = {
  id: 1,
  name: 'wang',
};

fetch('/modifyFn', {
  method: 'PUT',
  body: JSON.stringify(modifyFn),
  headers: {'Content-Type': 'application/json' }
}).then((res) => {
  return res.json()
}).then((res) => {
  console.log(res)
})

/**
 * @name findFn
 * @type {*[]}
 */
var findFn = {
  id: 1
};

fetch('/findFn', {
  method: 'GET',
  body: JSON.stringify(findFn),
  headers: {'Content-Type': 'application/json' }
}).then((res) => {
  return res.json()
}).then((res) => {
  console.log(res)
})

