/**
 * created by Tokey Jerry
 * mongoose config
 */

const gloableConfig = {
	url: {
		development: 'mongodb://127.0.0.1:27017/tjw',
		production: 'mongodb://127.0.0.1:27017/tjw'
	}
}

export default gloableConfig
