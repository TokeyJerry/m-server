# QUICK START

> nvm --version 0.33.11
> 
> node --version v8.12.0

## 1.Clone add Install

```sh
# Clone
git clone --depth=1 git@gitlab.com:TokeyJerry/m-server.git

# Install by yarn or npm
# yarn
yarn 

# or npm
npm install
```

## 2.Structure

```
.
├── app.js ·············· 入口文件
├── Controller ·········· 逻辑层
│   ├── test1.js
│   ├── BSfile
│   ├── ...
│   └── ...
├── Model ··············· 数据层
│   ├── test1.js
│   ├── DATAfile
│   ├── ...
│   └── ...
├── Router ·············· 路由
├── Docs ················ 文档
├── global.config.js ···· 全局配置文件
├── process.json ········ pm2 集成 发布配置文件
└── package.json
```

## 3.View Docs

```sh
# You can view documents of this project via shell
# yarn
yarn docs:dev

# or npm 
npm run docs:dev

# And then you can view `http://localhost:8081/`
```

## 4.Start mongodb

> You need to have a local database for start this project
> 
> If you do not know how to install mongodb,click [here](./Install_Mongodb.md)

```sh
# Start the local database
sudo mongod
```

## 5.Start project

```sh
# yarn
yarn start

# or npm 
npm start
```
