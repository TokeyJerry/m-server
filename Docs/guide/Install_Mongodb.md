
# Install MongoDB

> [Homebrew Tools](https://brew.sh/)

```sh
# 1.Install
brew install mongodb

# 2.Environment variable configuration to facilitate global directory startup
sudo vim ~/.bash_profile

# 3.Export MongoDB's startup path
export PATH=$PATH:/usr/local/Cellar/mongodb/[version]/bin

# 4.Create a data store directory
sodu mkdir /data/db

# 5.Start mongodb
sudo mongod
```
