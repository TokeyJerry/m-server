
# Koa 设置跨域

## 一、跨域原理剖析

> 欲想明确跨域原理，必先了解: 1️⃣.”简单请求“和”复杂请求“，2️⃣.`options`请求

### 1.Access-Control-Allow-Origin

> 常见错误：No 'Access-Control-Allow-Origin' header is present on the requested resource. Origin 'http://XXX' is therefore not allowed access

- 访问控制允许同源
- 出于安全的考虑，浏览器允许跨域写，而不允许跨域读，写就是上行，发送请求。
    + 表单默认提交（get、post）、超链接访问域外的资源，这是允许的，因为在点击按钮、超链接时，浏览器地址已经变了，这就是一个普通的请求，不存在跨域；
    + ajax(借助xmlhttprequest)跨域请求，这是被禁止的，因为ajax就是为了接受接受响应，这违背了，不允许跨域读的原则；
    + jsonp属于跨域读且形式限制为GET方式，它利用了script标签的特性；这是允许的。因为浏览器把跨域读脚本，当作例外，类似的img、iframe的src都可以请求域外资源
- 参数：`*` 、 `<Origin>`

### 2.Access-Control-Allow-Credentials

- 是否允许发送 Cookie
- 参数：`<Boolean>`




## 二、跨域拦截器

- 1、

```js
/**
 * 跨域拦截器
 * 基本模式
 */
router.all('*', async function(ctx, next) {
	/**
	 * 访问控制允许同源
	 * 出于安全的考虑，浏览器允许跨域写，而不允许跨域读，写就是上行，发送请求
	 */
	ctx.set("Access-Control-Allow-Origin", "*");    
	ctx.set("Access-Control-Allow-sets", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");    
	ctx.set("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");  
	ctx.set("Access-Control-Allow-Headers","x-requested-with, accept, origin, content-type");
	ctx.set("Access-Control-Allow-Credentials", false);  
	ctx.set("X-Powered-By",' 3.2.1'); 
	
	if(ctx.method =="OPTIONS") {
		ctx.status = 200;/*让options请求快速返回*/   
	} else {
		next();
	}
})

```

- 2、cors 模式

```js
/**
 * 跨域拦截器
 * cors 模式
 */
const cors = require('koa2-cors') // 跨域解决
app.use(cors({
	origin: function(ctx) {
		if (/\/api2/.test(ctx.url)) {
			return '*'
		}
		return 'http://localhost:3000' // 仅允许 http://localhost:3000 该域名跨域请求
	},
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  maxAge: 5,
  credentials: true,
  allowMethods: ['GET', 'POST', 'DELETE'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))
```

- 3、nginx 实现跨域拦截



## 五、参考文档

- [Access control allow origin 简单请求和复杂请求](https://blog.csdn.net/beyond__devil/article/details/78082585)

- [这个API很“迷人”——(新的Fetch API)
](http://www.open-open.com/lib/view/open1426815580164.html)

- [使用 Fetch MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch)

- [传统 Ajax 已死，Fetch 永生](https://github.com/camsong/blog/issues/2)

- [## JS 异步编程](https://github.com/NARUTOne/blog-note/issues/14)

- [浏览器跨域方法与基于Fetch的Web请求最佳实践](https://segmentfault.com/a/1190000006095018)

- [现代 Web 开发基础与工程实践](https://github.com/wxyyxc1992/Web-Series#advanced-2)

- [node.js 应答跨域请求实现](https://www.jianshu.com/p/5b3acded5182)

- [使用fetch post json数据的问题](https://blog.csdn.net/neoveee/article/details/54985418?utm_source=itdadao&utm_medium=referral)

- [跨域资源共享 CORS 详解](http://www.ruanyifeng.com/blog/2016/04/cors.html)

- [CORS跨域-Nginx使用方法](https://blog.csdn.net/rth362147773/article/details/78817868)











