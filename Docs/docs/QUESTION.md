
# QUESTIONS

## 一、pm2 questions

#### Q1：pm2 watch 配置了文件，修改文件下的 js，pm2 没有重新启动，文件不实时更新

**A1：** pm2 配置文件下加如下配置：

```json
"watch_options": {
  "usePolling": true
},
```

并且 kill pm2 进程，重新启动

```sh
# kill pm2 进程
pm2 kill

# 重新启动项目
pm2 start [配置文件]
```

#### Q2：pm2 查看控制台日志

```sh
# pm2 monit [appname] 该方法可以查找到 pm2 log 存放位置，从而查看日志
pm2 monit [appname]
# 实时查看
pm2 info [id|appname]
# 实时查看 以控制台方式查看
pm2 log [id|appname]
```

------

## 二、Koa questions

#### Q1：fetch 请求设置 `mode: 'no-cors'` 时，并且以 `res.json()` 接收，请求报错 `Uncaught (in promise) SyntaxError: Unexpected end of input`。

**A1：**

- 原因：在使用 fetch 时，如果设置 `mode : 'no-cors'` 将会导致 request Header 不可变，也即不可设置。从而引用浏览器默认的 content-type 属性 `text/plain`，而非设置的 `application/json`。

- 解决方法：将 `mode` 设置为 `cors` 即可。

- 提示：在使用 fetch post json 数据时，body 字段应该使用 JSON.stringify 序列化。


#### Q2：Koa bodyParser 参数获取

**A1：** Koa get 请求参数获取

```js
// eg:
router.get('/:id', async function (ctx, next) {
  console.log(ctx.query);
  console.log(ctx.querystring);
  console.log(ctx.request.query);
  console.log(ctx.request.querystring);
  ctx.body = 'this is a api get response!'
})
```

- query 参数
		```js
		let query = ctx.query
		// let query = ctx.request.query
		```

- params 参数
		```js
		let param = ctx.params
		```

**A2：** Koa post 请求参数获取

```js
// eg:
router.post('/add', async function (ctx, next) {
	console.log(ctx.request.body)
  ctx.body = ctx.request.body
})
```

- body 参数
		```js
		let body = ctx.request.body
		```

#### Q3：Koa 应用可使用几种中间件：

**A1：** Koa 应用可使用如下几种中间件：

- 应用级中间件
- 路由级中间件
- 错误处理中间件
- 第三方中间件
  



------



## 五、参考文档

- [Access control allow origin 简单请求和复杂请求](https://blog.csdn.net/beyond__devil/article/details/78082585)
- [Koa2中文文档](https://www.itying.com/koa/article-index-id-83.html)
- []()
- []()
- []()
- []()
- []()
- []()
- []()





