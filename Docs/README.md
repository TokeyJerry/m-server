---
home: true
actionText: QUICK START →
actionLink: /guide/
features:
  - title: 基础框架 Koa
    details: 更小、更富有表现力、更健壮
  - title: 数据库 MongoDB
    details: 介于关系数据库和非关系数据库之间，旨在为 WEB 应用提供可扩展的高性能数据存储解决方案
  - title: 数据库 MySQL
    details: 最流行的关系型数据库管理系统
footer: MIT Licensed | Authored by tiejianwen@guazi.com
---

### 整体目录结构

> 各功能目录请详细参考功能页面

```
.
├── app.js ·············· 入口文件
├── Controller ·········· 逻辑层
│   ├── test1.js
│   ├── BSfile
│   ├── ...
│   └── ...
├── Model ··············· 数据层
│   ├── test1.js
│   ├── DATAfile
│   ├── ...
│   └── ...
├── Router ·············· 路由
├── Docs ················ 文档
├── global.config.js ···· 全局配置文件
├── process.json ········ pm2 集成 发布配置文件
└── package.json
```

[MIT](LICENSE/README.md) &copy; [铁建文](http://platform.apptie.cn)
