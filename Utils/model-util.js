
/**
 * @auth Tie Jianwen
 * @class Model
 * @description
 */

class Model {
  /**
   * Create a model
   * @param {object} mongoose.model
   */
  constructor(model) {
    if (!model) {
      throw new Error('model param err');
    }
    this.model = model;
  }

  /**
   * @function ModelInstance.createBatch
   * @description 批量新增方法
   * @params {*} 需要存储的数据
   * @returns {promise}
   */
  createBatch(...data) {
    return new Promise((resolve, reject) => {
      this.model.create(...data, (err, ...result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  /**
   * @function ModelInstance.create
   * @description 新增方法
   * @params {*} 需要存储的数据
   * @returns {promise}
   */
  create(data={}) {
    this.document = new this.model(data);
    return new Promise((resolve, reject) => {
      this.document.save((err, result, numerAffect) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }
  // delete
  delete(...data) {
    return new Promise((resolve, reject) => {
      this.model.remove(...data, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }
  // update
  update() {

  }
  // select / find
  // count
};

export default Model;
