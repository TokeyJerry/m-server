/**
 * demo01Schema
 */

import mongoose from 'mongoose';
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

export const Demo01_Shema = new Schema({
  name: {
    type: String,
    require: true,
  },
  email: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  meta: {
    createAt: {
      type: Date,
      default: Date.now()
    },
    updateAt: {
      type: Date,
      default: Date.now()
    },
    deleteAt: {
      type: Date,
      default: Date.now()
    },
  }
});

Demo01_Shema.plugin(AutoIncrement, {inc_field: 'id'});

export default mongoose.model('Demo01', Demo01_Shema);
