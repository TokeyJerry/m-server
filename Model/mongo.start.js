/**
 * mongo start
 */

import mongoose from 'mongoose'
import gloableConfig from '../global.config.js'

const MONGO_URL = gloableConfig.url[process.env.NODE_ENV] || 'development'

export default () => new Promise((reslove, reject) => {
  mongoose.connect(MONGO_URL, { useNewUrlParser: true })

  // 连接成功
  mongoose.connection.on('connected', () => {
    console.log('Mongoose connection connected');
    reslove(mongoose.connections)
  });

  // 连接失败
  mongoose.connection.on('error', error => {
    console.log('Mongoose connection error');
    reject(error)
  });

  // 断开连接
  mongoose.connection.on('disconnected', () => {
    console.log('Mongoose connection disconnected');
    reject('Mongoose connection disconnected')
  });
})