# m-server

## Clone add Install

```sh
# Clone
git clone --depth=1 git@gitlab.com:TokeyJerry/m-server.git

# Install by yarn or npm
# yarn
yarn 

# or npm
npm install
```

## Structure

```
.
├── app.js ·············· 入口文件
├── Controller ·········· 逻辑层
│   ├── test1.js
│   ├── BSfile
│   ├── ...
│   └── ...
├── Model ··············· 数据层
│   ├── test1.js
│   ├── DATAfile
│   ├── ...
│   └── ...
├── Router ·············· 路由
├── Docs ················ 文档
├── global.config.js ···· 全局配置文件
├── process.json ········ pm2 集成 发布配置文件
└── package.json
```

## Introduction

## Usage

### 1.View Docs
      
```sh
# You can view documents of this project via shell
# yarn
yarn docs:dev

# or npm 
npm run docs:dev

# And then you can view `http://localhost:8081/`
```

### 2.Usage

[Please view Docs /::D](./Docs/guide/README.md)

## License

[MIT](LICENSE) &copy; [铁建文](http://platform.apptie.cn)
